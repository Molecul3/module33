import json
import pandas as pd
import dill
import logging
import os
from datetime import datetime
from fastapi import FastAPI
from pydantic import BaseModel
from os import walk

path = os.environ.get('PROJECT_PATH', '..')
filenames = next(walk('../data/test'), (None, None, []))[2]

app = FastAPI()
with open('C:/Users/фвьшт/airflow_hw/data/models/cars_pipe_202301052201.pkl', 'rb') as file:
    model = dill.load(file)


class Form(BaseModel):
    description: str
    fuel: str
    id: int
    image_url: str
    lat: float
    long: float
    manufacturer: str
    model: str
    odometer: float
    posting_date: str
    price: int
    region: str
    region_url: str
    state: str
    title_status: str
    transmission: str
    url: str
    year: float


class Prediction(BaseModel):
    id: str
    price: int
    price_category: str


# @app.post('/predict', response_model=Prediction)
def predict():
    df_pred = pd.DataFrame()
    for filename in filenames:
        filename = '../data/test/' + filename
        with open(filename) as data_file:
            data = json.load(data_file)
        df = pd.json_normalize(data)
        y = model.predict(df)
        df_append = pd.Series(y)
        df_pred = pd.concat([df_pred, df_append])
        logging.warning("next step")
    prediction_filename = f'{path}/data/predictions/predictions_{datetime.now().strftime("%Y%m%d%H%M")}.csv'
    df_pred.to_csv(prediction_filename, index=False)

    return 'predictions downloaded to file predictions.csv'


if __name__ == '__main__':
    predict()
